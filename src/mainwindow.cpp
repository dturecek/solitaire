/**
 * @file mainwindow.cpp
 * @brief This file contains the implementation of methods used in graphical
 *        interface.
 *
 * @author Dominik Tureček, xturec06@stud.fit.vutbr.cz
 */
#include <QDesktopWidget>
#include <QMainWindow>
#include <QSize>
#include <QMenuBar>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qpainter.h"

void GameTab::handleUndo() {
    game.undo();
    update();
}

void GameTab::handleSave() {
    QFileDialog fileDialog;
    fileDialog.setWindowFlags(Qt::WindowStaysOnTopHint);
    QString file = fileDialog.getSaveFileName(this, tr("Save Game"), "", "");
    game.saveGame(file.toStdString());
}

void GameTab::handleLoad() {
    QFileDialog fileDialog;
    fileDialog.setWindowFlags(Qt::WindowStaysOnTopHint);
    QString file = fileDialog.getOpenFileName(this, tr("Load Game"), "", "");
    if (game.loadGame(file.toStdString()))
        if (!game.inProgress()) {
            MainWindow* w = NULL;
            foreach(QWidget *widget, qApp->topLevelWidgets())
                if(widget->inherits("QMainWindow")) {
                    w = qobject_cast<MainWindow*>(widget);
                    break;
                }

            if (!w->isResized())
                w->resizeWindows();
        }
    update();
}

void GameTab::handleNewGame() {
    if (!game.inProgress()) {
        MainWindow* w = NULL;
        foreach(QWidget *widget, qApp->topLevelWidgets())
            if(widget->inherits("QMainWindow")) {
                w = qobject_cast<MainWindow*>(widget);
                break;
            }
       if (!w->isResized())
            w->resizeWindows();
    }
    game.startGame();
    update();
}

void GameTab::handleHint() {
    std::string hint;
    game.printHint(hint);
    QMessageBox msgBox;
    msgBox.setStyleSheet("background:white");
    QString str = QString::fromUtf8(hint.c_str());
    msgBox.setText(str);
    msgBox.setWindowTitle("Hint");
    msgBox.setIcon(QMessageBox::Question);
    msgBox.exec();
}

void GameTab::handleQuit() {
    game.quitGame();
    update();
}

void GameTab::startGame() {
    game.startGame();
    update();
}

GameTab::GameTab(QDockWidget *parent) :
    QDockWidget(parent) {
    resize(QDesktopWidget().availableGeometry(this).size() * 0.9);
    setWindowTitle("Solitaire");

    menu = new QMenuBar(this);
    setStyleSheet("background:rbg(0.1, 140.0/255, 0.1, 0.8)");

    mainMenu = new QMenu("Menu");
    QAction* saveAction = mainMenu->addAction("Save");
    QAction* loadAction = mainMenu->addAction("Load");
    QAction* undoAction = mainMenu->addAction("Undo");
    QAction* hintAction = mainMenu->addAction("Hint");
    QAction* newGameAction = mainMenu->addAction("Start New Game");
    QAction* quitAction = mainMenu->addAction("Quit");

    menu->addMenu(mainMenu);

    connect(undoAction, SIGNAL(triggered()),this,SLOT(handleUndo()));
    connect(saveAction, SIGNAL(triggered()),this,SLOT(handleSave()));
    connect(loadAction, SIGNAL(triggered()),this,SLOT(handleLoad()));
    connect(newGameAction, SIGNAL(triggered()),this,SLOT(handleNewGame()));
    connect(hintAction, SIGNAL(triggered()),this,SLOT(handleHint()));
    connect(quitAction, SIGNAL(triggered()),this,SLOT(handleQuit()));

    setFeatures(QDockWidget::NoDockWidgetFeatures);
    show();
}

void GameTab::closeEvent(QCloseEvent * event) {
    event->ignore();
}

void GameTab::paintEvent(QPaintEvent * event) {
    (void) event;
    QPainter painter(this);
    QRect frame;
    QSize windowSize = size();
    int offset = windowSize.height()/20;
    int cardHeight = windowSize.height() / 6;
    int cardWidth = (int) (72.0 / 96 * cardHeight);
    QImage picture;
    State state = game.getGameState();

    // Background
    frame.setRect(0, 0, windowSize.width(), windowSize.height());
    painter.fillRect(frame, QColor::fromRgbF(0.1, 140.0/255, 0.1, 0.8));

    // Paint main deck
    if (state.mainDeckEmpty)
        picture.load("src/img/place.png");
    else
        picture.load("src/img/cardback.png");

    mainDeck.x1 = cardWidth/5;
    mainDeck.x2 = cardWidth*6/5;
    mainDeck.y1 = cardHeight/5 + offset;
    mainDeck.y2 = cardHeight*6/5 + offset;
    frame.setRect(mainDeck.x1, mainDeck.y1, cardWidth, cardHeight);
    painter.drawImage(frame, picture);

    // Paint the deck
    if (state.drawDeckEmpty)
        picture.load("src/img/place.png");
    else {
        Cards card = state.drawDeckTopCard;
        std::string name = "src/img/" + std::to_string(card.color) + std::to_string(card.value) + ".svg";

        picture.load(name.c_str());
    }
    drawDeck.x1 = cardWidth*7/5;
    drawDeck.x2 = cardWidth*12/5;
    drawDeck.y1 = cardHeight/5 + offset;
    drawDeck.y2 = cardHeight*6/5 + offset;
    frame.setRect(drawDeck.x1, drawDeck.y1, cardWidth, cardHeight);
    painter.drawImage(frame, picture);

    // Paint foundations
    for (int i = 0; i < 4; ++i) {
        if (state.resultDeckEmpty[i]) {
            std::string name = "src/img/b" + std::to_string(i) + ".png";
            picture.load(name.c_str());
        }
        else {
            Cards card = state.resultDecksTopCards[i];
            std::string name = "src/img/" + std::to_string(i) + std::to_string(card.value) + ".svg";
            picture.load(name.c_str());
        }
        resultDeck[i].x1 = cardWidth*(19+6*i)/5;
        resultDeck[i].x2 = cardWidth*(24+6*i)/5;
        resultDeck[i].y1 = cardHeight/5 + offset;
        resultDeck[i].y2 = cardHeight*6/5 + offset;
        frame.setRect(resultDeck[i].x1, resultDeck[i].y1, cardWidth, cardHeight);
        painter.drawImage(frame, picture);
    }

    // Paint piles
    for (int i = 0; i < 7; ++i) {
        tableDeck[i].wholeDeck.x1 = cardWidth*(1+6*i)/5;
        tableDeck[i].wholeDeck.x2 = cardWidth*(6+6*i)/5;
        tableDeck[i].wholeDeck.y1 = cardHeight*7/5 + offset;
        tableDeck[i].wholeDeck.y2 = cardHeight*12/5 + offset;

        if (state.tableDecks[i].isEmpty()) {
            picture.load("src/img/place.png");
            tableDeck[i].dy = 0;
            tableDeck[i].lastCardY1 = tableDeck[i].wholeDeck.y1;
            frame.setRect(tableDeck[i].wholeDeck.x1, tableDeck[i].wholeDeck.y1, cardWidth, 
                          cardHeight);
            painter.drawImage(frame, picture);
        }
        else {
            tableDeck[i].dy = cardHeight/5;
            tableDeck[i].wholeDeck.y2 += (state.tableDecks[i].countAllCards()-1) * tableDeck[i].dy;
            tableDeck[i].lastCardY1 = tableDeck[i].wholeDeck.y2 - cardHeight;
            int faceDown = state.tableFaceDownCount[i];
            picture.load("src/img/cardback.png");
            for (int j = 0; j < faceDown; ++j) {

                frame.setRect(tableDeck[i].wholeDeck.x1,tableDeck[i].wholeDeck.y1 \
                              + tableDeck[i].dy*(j), cardWidth, cardHeight);
                painter.drawImage(frame, picture);
            }
            std::vector< struct Cards > cards;
            state.tableDecks[i].getXCards(state.tableDecks[i].countAllCards() - faceDown, cards);
            for (int n = 0; n < state.tableDecks[i].countAllCards() - faceDown; ++n) {
                std::string name = "src/img/" + std::to_string(cards[n].color) \
                                   + std::to_string(cards[n].value) + ".svg";
                picture.load(name.c_str());
                frame.setRect(tableDeck[i].wholeDeck.x1, tableDeck[i].wholeDeck.y1 \
                              + tableDeck[i].dy*(faceDown+n), cardWidth, cardHeight);
                painter.drawImage(frame, picture);
            }
        }
    }
}

GameTab::~GameTab() {
    delete mainMenu;
    delete menu;
}

void MainWindow::resizeWindows() {
    QDockWidget *dockWidget = qobject_cast<QDockWidget*>(&window0);
    dockWidget->setMinimumWidth(width() * 0.5);
    dockWidget->setMinimumHeight(height() * 0.5);
    dockWidget->show();
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, dockWidget);

    dockWidget = qobject_cast<QDockWidget*>(&window1);
    dockWidget->setMinimumWidth(width() * 0.5);
    dockWidget->setMinimumHeight(height() * 0.5);
    dockWidget->show();
    dockWidget->setAllowedAreas(Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, dockWidget);

    dockWidget = qobject_cast<QDockWidget*>(&window2);
    dockWidget->setMinimumWidth(width() * 0.5);
    dockWidget->setMinimumHeight(height() * 0.5);
    dockWidget->show();
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, dockWidget);

    dockWidget = qobject_cast<QDockWidget*>(&window3);
    dockWidget->setMinimumWidth(width() * 0.5);
    dockWidget->setMinimumHeight(height() * 0.5);
    dockWidget->show();
    dockWidget->setAllowedAreas(Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, dockWidget);

    gameWindowsResized = true;
}

bool MainWindow::isResized() {
    return gameWindowsResized;
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    resize(QDesktopWidget().availableGeometry(this).width(),
           QDesktopWidget().availableGeometry(this).width());
    statusBar()->hide();
    setWindowTitle("Solitaire");
    delete ui->mainToolBar;

    QDockWidget *dockWidget = qobject_cast<QDockWidget*>(&window0);
    dockWidget->setMinimumWidth(width() * 0.8);
    dockWidget->setMinimumHeight(height() * 0.3);
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
    window0.startGame();
    dockWidget->setWindowFlags ( Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    dockWidget->show();

    dockWidget = qobject_cast<QDockWidget*>(&window1);
    dockWidget->setMinimumWidth(width() * 0.05);
    dockWidget->setAllowedAreas(Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, dockWidget);
    dockWidget->setWindowFlags ( Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    dockWidget->show();

    dockWidget = qobject_cast<QDockWidget*>(&window2);
    dockWidget->setMinimumWidth(width() * 0.95);
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
    dockWidget->show();

    dockWidget = qobject_cast<QDockWidget*>(&window3);
    dockWidget->setMinimumWidth(width() * 0.05);
    dockWidget->setAllowedAreas(Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, dockWidget);
    dockWidget->show();

    setStyleSheet("background:lightGrey");
    show();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent*) {
    qApp->quit();
}

bool GameTab::identifyClick(int x, int y) {
    // Upper line of decks
    if (y >= drawDeck.y1 && y <= drawDeck.y2) {
        // The deck
        if (x >= drawDeck.x1 && x <= drawDeck.x2) {
            lastClick = "d";
            return true;
        }
        // Foundations
        for (int i = 0; i < 4; ++i) {
            if (x >= resultDeck[i].x1 && x <= resultDeck[i].x2) {
                lastClick = "r" + std::to_string(i+1);
                cardCount = 1;
                return true;
            }
        }
        return false;
    }

    // Piles
    for (int i = 0; i < 7; ++i) {
        if (x >= tableDeck[i].wholeDeck.x1 && x <= tableDeck[i].wholeDeck.x2
                && y >= tableDeck[i].wholeDeck.y1 && y <= tableDeck[i].wholeDeck.y2) {
            lastClick = "t" + std::to_string(i+1);
            if (y >= tableDeck[i].lastCardY1)
                 cardCount = 1;
            else
                cardCount = (tableDeck[i].lastCardY1-y)/tableDeck[i].dy + 2;
            return true;
        }
    }
    return false;
}

void GameTab::mousePressEvent(QMouseEvent * event) {
    if (event->button() != Qt::LeftButton)
        return;

    int clickX = event->x();
    int clickY = event->y();

    // Click on main deck -> flip action
    if (clickX >= mainDeck.x1 && clickX <= mainDeck.x2
            && clickY >= mainDeck.y1 && clickY <= mainDeck.y2) {
        game.handleInput("flip");
        update();
    }
    else if (!identifyClick(clickX, clickY))
        lastClick = "none";
}

void GameTab::mouseReleaseEvent(QMouseEvent * event) {
    if (event->button() != Qt::LeftButton)
        return;

    if (lastClick.compare("none") == 0)
        return;

    QMessageBox msgBox;
    msgBox.setStyleSheet("background:white");
    msgBox.setText("You Win! Congratulations!");
    msgBox.setWindowTitle("Win");
    msgBox.setIcon(QMessageBox::Information);

    int clickX = event->x();
    int clickY = event->y();

    // Move to foundation
    if (clickY >= resultDeck[0].y1 && clickY <= resultDeck[0].y2) {
        for (int i = 0; i < 4; ++i) {
            if (clickX >= resultDeck[i].x1 && clickX <= resultDeck[i].x2) {
                if (lastClick.compare("d") == 0) {
                    game.handleInput("dr" + std::to_string(i+1));
                    update();
                    if (game.checkWin())
                        msgBox.exec();

                    lastClick = "none";
                    return;
                }
                else {
                    std::string cmd = lastClick + "r" + std::to_string(i+1) + ":" \
                                      + std::to_string(cardCount);
                    game.handleInput(cmd);
                    update();
                    if (game.checkWin())
                        msgBox.exec();

                    lastClick = "none";
                    return;
                }
            }
        }
    }
    // Move to pile
    for (int i = 0; i < 7; ++i) {
        if (clickX >= tableDeck[i].wholeDeck.x1 && clickX <= tableDeck[i].wholeDeck.x2
                && clickY >= tableDeck[i].wholeDeck.y1 && clickY <= tableDeck[i].wholeDeck.y2) {
            if (lastClick.compare("d") == 0) {
                game.handleInput("dt" + std::to_string(i+1));
                update();
                return;
            }
            else {
                std::string cmd = lastClick + "t" + std::to_string(i+1) + ":" \
                                  + std::to_string(cardCount);
                game.handleInput(cmd);
                update();
                return;
            }
        }
    }
}
/** End of file mainwindow.cpp **/
