/**
 * @file game.cpp
 * @brief This file contains implmentation of functions and class methods for
 * game representation.
 *
 * @author Dominik Tureček, xturec06@stud.fit.vutbr.cz
 */
#include <fstream>
#include "game.hpp"

std::string getCardName(Cards card) {
    std::string name;

    if (card.value == ace)
        name = "Ace of ";
    else if (card.value >= 2 && card.value <= 10)
        name = std::to_string(card.value) + " of ";
    else if (card.value == jack)
        name = "Jack of ";
    else if (card.value == queen)
        name = "Queen of ";
    else if (card.value == king)
        name = "King of ";

    if (card.color == hearts)
        name += "Hearts";
    else if (card.color == diamonds)
        name += "Diamonds";
    else if (card.color == spades)
        name += "Spades";
    else if (card.color == clubs)
        name += "Clubs";

    return name;
}

bool Game::inProgress() {
    return gameInProgress;
}

void Game::startGame() {
    quitGame();
    gameInProgress = true;

    mainDeck.createMainDeck();
    mainDeck.shuffleDeck();

    for (int i = 0; i < 7; ++i) {
        for (int j = i; j < 7; ++j) {
            mainDeck.dealCard(tableDecks[j]);
        }
        tableDecks[i].flipTopCard();
    }
}

void Game::quitGame() {
    mainDeck.clearDeck();
    drawDeck.clearDeck();
    for (int i = 0; i < 4; ++i)
        resultDecks[i].clearDeck();
    for (int i = 0; i < 7; ++i)
        tableDecks[i].clearDeck();
    steps.clear();
}

bool Game::checkWin() {
    for (int i = 0; i < 4; ++i) {
        if (resultDecks[i].isEmpty())
            return false;
        if (resultDecks[i].getTopCard().value != king)
            return false;
    }
    return true;
}

void Game::printGame() {
    std::cout << "==========================================================\n";
    if (mainDeck.isEmpty()) {
        mainDeck.printDeck();
        std::cout << "\n";
    }

    std::cout << "Draw deck [d]:\t\t";
    drawDeck.printTopCard();
    std::cout << "\n";

    std::cout << "==========================================================\n";
    for (int i = 0; i < 7; ++i) {
        std::cout << "Table deck " << i+1 << " [t" << i+1 << "]:\t";
        tableDecks[i].printDeck();
        std::cout << "\n";
    }

    std::cout << "==========================================================\n";
    // hearts
    std::cout << "Result deck \xe2\x99\xa5 [r1]:\t";
    resultDecks[0].printTopCard();
    std::cout << "\n";
    // diamonds
    std::cout << "Result deck \xe2\x99\xa6 [r2]:\t";
    resultDecks[1].printTopCard();
    std::cout << "\n";
    // spades
    std::cout << "Result deck \xe2\x99\xa0 [r3]:\t";
    resultDecks[2].printTopCard();
    std::cout << "\n";
    // clubs
    std::cout << "Result deck \xe2\x99\xa3 [r4]:\t";
    resultDecks[3].printTopCard();
    std::cout << "\n";
    std::cout << "==========================================================\n";
}

State Game::getGameState() {
    State output;
    output.mainDeckEmpty = mainDeck.isEmpty();
    output.drawDeckEmpty = drawDeck.isEmpty();

    if (!output.drawDeckEmpty)
        output.drawDeckTopCard = drawDeck.getTopCard();

    for (int i = 0; i < 4; ++i) {
        if ((output.resultDeckEmpty[i] = resultDecks[i].isEmpty()))
            continue;
        output.resultDecksTopCards[i] = resultDecks[i].getTopCard();
    }

    for (int i = 0; i < 7; ++i) {
        output.tableFaceDownCount[i] = tableDecks[i].countAllCards() \
                                       - tableDecks[i].countFaceUpCards();
        output.tableDecks[i] = tableDecks[i];
    }
    return output;
}

bool identifyDeck(std::string name, int* type, int* index) {
    if (name.compare("d") == 0) {
        *type = draw;
        return true;
    }
    else if (name.substr(0,1).compare("t") == 0) {
        *type = table;
    }
    else if (name.substr(0,1).compare("r") == 0) {
        *type = result;
    }
    else {
        return false;
    }

    if (name.length() == 2) {
        try {
            *index = std::stoi(name.substr(1,1));
            if (*type == table && *index >= 1 && *index <= 7)
                return true;
            else if (*type == result && *index >= 1 && *index <= 4)
                return true;
            else
                return false;
        }
        catch(...) {
            return false;
        }
    }
    else
        return false;
}

bool checkStraight(std::vector<Cards> arr, int count) {
    for (int i = 0; i < count-1; ++i) {
        if (arr[i].value - 1 != arr[i+1].value)
            return false;
        if (arr[i].color/2 == arr[i+1].color/2)
            return false; 
    }

    return true;
}

bool checkCombination(int src, int dest, int count, int idxS, int idxD) {
    if (src == dest && idxS == idxD)
        return false;
    else if (src == draw && count != 1)
        return false;
    else if (dest == draw)
        return false;
    else if ((src == result || dest == result) && count != 1)
        return false;

    return true;
}

bool Game::checkAndDeal(std::string src, std::string dest, int count) {
    int typeS, typeD;
    int indexS = 0;
    int indexD = 0;
    Action step;

    if (!identifyDeck(src, &typeS, &indexS) || !identifyDeck(dest, &typeD, &indexD)) {
        return false;
    }
    else if (!checkCombination(typeS, typeD, count, indexS, indexD)) {
        return false;
    }

    --indexD;
    --indexS;

    step.flip = false;
    step.fromType = typeS;
    step.toType = typeD;
    step.fromIndex = indexS;
    step.toIndex = indexD;
    step.count = count;
    step.flippedTopCard = false;

    // Move from the deck
    if (typeS == draw && !drawDeck.isEmpty()) {
        Cards drawCard = drawDeck.getTopCard();

        // Move to a foundation
        if (typeD == result) {
            if (drawCard.color == indexD) {
                if (resultDecks[indexD].isEmpty()) {
                    if (drawCard.value == ace)
                        drawDeck.dealCard(resultDecks[indexD]);
                    else
                        return false;
                }
                else {
                    Cards resultCard = resultDecks[indexD].getTopCard();
                    if (drawCard.value - 1 == resultCard.value) {
                        drawDeck.dealCard(resultDecks[indexD]);
                    }
                    else
                        return false;
                }
            }
            else
                return false;
        }

        // Move to a pile
        else if (typeD == table) {
            Cards drawCard = drawDeck.getTopCard();
            if (tableDecks[indexD].isEmpty()) {
                if (drawCard.value == king)
                    drawDeck.dealCard(tableDecks[indexD]);
                else
                    return false;
            }
            else {
                Cards tableCard = tableDecks[indexD].getTopCard();
                if (!tableCard.faceUp)
                    return false;
                if (drawCard.color/2 != tableCard.color/2 && drawCard.value+1 == tableCard.value)
                    drawDeck.dealCard(tableDecks[indexD]);
                else
                    return false;
            }
        }
    }

    // Move from a pile
    else if (typeS == table && !tableDecks[indexS].isEmpty()) {
        std::vector<Cards> tableCards;

        if (tableDecks[indexS].getXCards(count, tableCards)) {
            if (!(count == 1 || checkStraight(tableCards, count)))
                return false;

            Cards movedCard = tableCards[0];
            Cards destCard;

            // Move to a pile
            if (typeD == table) {
                if (tableDecks[indexD].isEmpty()) {
                    if (movedCard.value == king)
                        tableDecks[indexS].dealXCards(tableDecks[indexD],count);
                    else
                        return false;
                }
                else {
                    destCard = tableDecks[indexD].getTopCard();
                    if (!destCard.faceUp)
                        return false;
                    if (movedCard.color/2 != destCard.color/2
                        && movedCard.value+1 == destCard.value) {
                        tableDecks[indexS].dealXCards(tableDecks[indexD],count);
                    }
                    else
                        return false;
                }
            }
            // Move to a foundation
            else if (typeD == result) {
                if (movedCard.color != indexD)
                    return false;
                if (resultDecks[indexD].isEmpty()) {
                    if (movedCard.value != ace)
                        return false;
                    else
                        tableDecks[indexS].dealCard(resultDecks[indexD]);
                }
                else {
                    destCard = resultDecks[indexD].getTopCard();
                    if (movedCard.value -1 == destCard.value)
                        tableDecks[indexS].dealCard(resultDecks[indexD]);
                    else
                        return false;
                }
            }
            else
                return false;
        }
        else {
            return false;
        }
        if (!tableDecks[indexS].isEmpty()) {
            Cards topCard = tableDecks[indexS].getTopCard();
            if (!topCard.faceUp) {
                tableDecks[indexS].flipTopCard();
                step.flippedTopCard = true;
            }
        }
    }

    // Move from a foundation
    else if (typeS == result && !resultDecks[indexS].isEmpty()) {

        // Move to a pile
        if (typeD == table) {
            Cards movedCard = resultDecks[indexS].getTopCard();
            if (tableDecks[indexD].isEmpty()) {
                if (movedCard.value == king) {
                    resultDecks[indexS].dealCard(tableDecks[indexD]);
                }
                else
                    return false;
            }
            else {
                Cards destCard = tableDecks[indexD].getTopCard();
                if (movedCard.color/2 != destCard.color/2 && movedCard.value+1 == destCard.value) {
                    resultDecks[indexS].dealCard(tableDecks[indexD]);
                }
                else
                    return false;
            }
        }
        else
            return false;
    }
    else
        return false;

    saveAction(step);
    return true;
}

std::string Game::printHint() {
    std::string output;
    Cards topCard;
    Cards movedCard;
    static int i = 0;

    // Move from the deck
    if (!drawDeck.isEmpty()) {
        movedCard = drawDeck.getTopCard();

        // To foundation
        if (!resultDecks[movedCard.color].isEmpty()) {
            if (resultDecks[movedCard.color].getTopCard().value+1 == movedCard.value) {
                output = ">>> dr" + std::to_string(movedCard.color+1);
                return output;
            }
        }
        else if (movedCard.value == ace) {
            output = ">>> dr" + std::to_string(movedCard.color+1);
            return output;
        }

        // To pile
        for (int i = 0; i < 7; ++i) {
            if (tableDecks[i].isEmpty()) {
                if (movedCard.value == king) {
                    output = ">>> dt" + std::to_string(i+1);
                    return output;
                }
                continue;
            }

            topCard = tableDecks[i].getTopCard();
            if (movedCard.color/2 != topCard.color/2 && movedCard.value+1 == topCard.value) {
                output = ">>> dt" + std::to_string(i+1);
                return output;
            }
        }
    }

    // Move from a pile
    int tmp = i;
    do {
        if (tableDecks[i].isEmpty()) {
            i = (i+1)%7;
            continue;
        }

        movedCard = tableDecks[i].getTopCard();

        // To foundation
        if (!resultDecks[movedCard.color].isEmpty()) {
            if (resultDecks[movedCard.color].getTopCard().value+1 == movedCard.value) {
                output = ">>> t" + std::to_string(i+1) + "r" + std::to_string(movedCard.color+1) \
                         + ":1";
                i = (i+1)%7;
                return output;
            }
        }
        else if (movedCard.value == ace) {
            output = ">>> t" + std::to_string(i+1) + "r" + std::to_string(movedCard.color+1) \
                     + ":1";
            i = (i+1)%7;
            return output;
        }

        // To pile
        int size = tableDecks[i].countFaceUpCards();
        std::vector<Cards> tableCards;
        for (int n = size; n > 0; --n) {
            tableCards.clear();
            if (!tableDecks[i].getXCards(n, tableCards)){
                continue;
            }

            if (!(n == 1 || checkStraight(tableCards, n))){
                continue;
            }

            movedCard = tableCards[0];

            for (int j = 0; j < 7; ++j) {
                if (tableDecks[j].isEmpty()) {
                    if (movedCard.value == king && n != tableDecks[i].countAllCards()) {
                        output = ">>> t" + std::to_string(i+1) + "t" + std::to_string(j+1) + ":" \
                                 + std::to_string(n);
                        i = (i+1)%7;
                        return output;
                    }
                    continue;
                }

                topCard = tableDecks[j].getTopCard();
                if (movedCard.color/2 != topCard.color/2 && movedCard.value+1 == topCard.value) {
                    output = ">>> t" + std::to_string(i+1) + "t" + std::to_string(j+1) + ":" \
                             + std::to_string(n);
                    i = (i+1)%7;
                    return output;
                }
            }
        }
    i = (i+1)%7;
    } while (i != tmp);

    if (!(mainDeck.isEmpty() && drawDeck.isEmpty())) {
        output = ">>> f";
        return output;
    }
    else {
        output = "No possible move found.";
        return output;
    }
}

std::string Game::printHint(std::string &longHint) {
    std::string output;
    Cards topCard;
    Cards movedCard;
    static int i = 0;

    // Move from the deck
    if (!drawDeck.isEmpty()) {
        movedCard = drawDeck.getTopCard();

        // To foundation
        if (!resultDecks[movedCard.color].isEmpty()) {
            topCard = resultDecks[movedCard.color].getTopCard();
            if (topCard.value+1 == movedCard.value) {
                output = ">>> dr" + std::to_string(movedCard.color+1);
                longHint = "Move " + getCardName(movedCard) + " from the deck onto " \
                           + getCardName(topCard) + " into the foundation.";
                return output;
            }
        }
        else if (movedCard.value == ace) {
            output = ">>> dr" + std::to_string(movedCard.color+1);
            longHint = "Move " + getCardName(movedCard) + " from the deck into the foundation.";
            return output;
        }


        // To a pile
        for (int i = 0; i < 7; ++i) {
            if (tableDecks[i].isEmpty()) {
                if (movedCard.value == king) {
                    output = ">>> dt" + std::to_string(i+1);
                    longHint = "Move " + getCardName(movedCard) + " from the deck into pile " \
                               + std::to_string(i+1) + ".";
                    return output;
                }
                continue;
            }

            topCard = tableDecks[i].getTopCard();
            if (movedCard.color/2 != topCard.color/2 && movedCard.value+1 == topCard.value) {
                output = ">>> dt" + std::to_string(i+1);
                longHint = "Move " + getCardName(movedCard) + " from the deck onto " \
                           + getCardName(topCard) + " into the pile " + std::to_string(i+1) + ".";
                return output;
            }
        }
    }

    // Move from a pile
    int tmp = i;
    do {
        if (tableDecks[i].isEmpty()) {
            i = (i+1)%7;
            continue;
        }

        movedCard = tableDecks[i].getTopCard();

        // To foundation
        if (!resultDecks[movedCard.color].isEmpty()) {
            topCard = resultDecks[movedCard.color].getTopCard();
            if (topCard.value+1 == movedCard.value) {
                output = ">>> t" + std::to_string(i+1) + "r" + std::to_string(movedCard.color+1) \
                         + ":1";
                longHint = "Move " + getCardName(movedCard) + " from pile " + std::to_string(i+1) \
                           + " onto " + getCardName(topCard) + " into the foundation.";
                i = (i+1)%7;
                return output;
            }
        }
        else if (movedCard.value == ace) {
            output = ">>> t" + std::to_string(i+1) + "r" + std::to_string(movedCard.color+1) + ":1";
            longHint = "Move " + getCardName(movedCard) + " from pile " + std::to_string(i+1) \
                       + " into the foundation.";
            i = (i+1)%7;
            return output;
        }

        // To a pile
        int size = tableDecks[i].countFaceUpCards();
        std::vector<Cards> tableCards;
        for (int n = size; n > 0; --n) {
            tableCards.clear();
            if (!tableDecks[i].getXCards(n, tableCards)){
                continue;
            }

            if (!(n == 1 || checkStraight(tableCards, n))){
                continue;
            }

            movedCard = tableCards[0];

            for (int j = 0; j < 7; ++j) {
                if (tableDecks[j].isEmpty()) {
                    if (movedCard.value == king && n != tableDecks[i].countAllCards()) {
                        output = ">>> t" + std::to_string(i+1) + "t" + std::to_string(j+1) \
                                 + ":" + std::to_string(n);
                        if (n == 1)
                            longHint = "Move " + getCardName(movedCard) + " from pile " \
                                       + std::to_string(i+1) + " into empty pile " \
                                       + std::to_string(j+1) + ".";
                        else
                            longHint = "Move " + std::to_string(n) + " cards from pile " \
                                       + std::to_string(i+1) + " into empty pile " \
                                       + std::to_string(j+1) + ".";
                        i = (i+1)%7;
                        return output;
                    }
                    continue;
                }

                topCard = tableDecks[j].getTopCard();
                if (movedCard.color/2 != topCard.color/2 && movedCard.value+1 == topCard.value) {
                    output = ">>> t" + std::to_string(i+1) + "t" + std::to_string(j+1) \
                             + ":" + std::to_string(n);
                    if (n == 1)
                        longHint = "Move " + getCardName(movedCard) + " from pile " \
                                   + std::to_string(i+1) + " onto " + getCardName(topCard) \
                                   + " in pile " + std::to_string(j+1) + ".";
                    else
                        longHint = "Move " + std::to_string(n) + " cards from pile " \
                                   + std::to_string(i+1) + " onto " + getCardName(topCard) \
                                   + " in pile " + std::to_string(j+1) + ".";
                    i = (i+1)%7;
                    return output;
                }
            }
        }
    i = (i+1)%7;
    } while (i != tmp);

    if (!(mainDeck.isEmpty() && drawDeck.isEmpty())) {
        output = ">>> f";
        longHint = "Deal new card from the deck.";
        return output;
    }
    else {
        output = "No possible move found.";
        longHint = output;
        return output;
    }
}

std::string Game::handleInput(std::string input) {
    std::string output;
    Action step;
    step.flip = false;

    // Deal new card to the deck
    if (input.compare("f") == 0 || input.compare("flip") == 0) {
        if (!(mainDeck.isEmpty() && drawDeck.isEmpty())) {
            step.flip = true;
            saveAction(step);
        }

        // If main Deck is not empty, top card is flipped and moved aside
        if (!mainDeck.isEmpty()) {
            mainDeck.dealCard(drawDeck);
            drawDeck.flipTopCard();
        }
        // If main deck is empty, all cards from the deck are flipped and moved
        // back
        else {
            while (!drawDeck.isEmpty()) {
                drawDeck.dealCard(mainDeck);
                mainDeck.flipTopCard();
            }
        }
    }
    // Print hint
    else if (input.compare("h") == 0 || input.compare("hint") == 0) {
        return printHint();
    }
    // Move card from the deck
    else if (input.substr(0,1).compare("d") == 0 && input.length() == 3) {
        std::string dest = input.substr(1,2);
        if (!checkAndDeal("d", dest, 1))
            output = "Invalid command [d<dst>].";
    }
    // Quit game
    else if (input.compare("q") == 0 || input.compare("quit") == 0) {
        output = "quit";
        return output;
    }
    // Save game
    else if (input.substr(0,2).compare("s:") == 0 || input.substr(0,5).compare("save:") == 0) {
        std::string fileName = input.substr(input.find(":")+1, std::string::npos);
        if (saveGame(fileName))
            output = "Successfully saved to a file " + fileName;
        else
            output = "Unsuccessful save to a file " + fileName;
    }
    // Load game
    else if (input.substr(0,2).compare("l:") == 0 || input.substr(0,5).compare("load:") == 0) {
        std::string fileName = input.substr(input.find(":")+1, std::string::npos);
        if (loadGame(fileName))
            output = "Successfully loaded from a file " + fileName;
        else
            output = "Unsuccessful load from a file " + fileName;
    }
    // Undo
    else if (input.compare("u") == 0 || input.compare("undo") == 0) {
        undo();
    }
    else {
        // Move a card from source to destination
        if (input.length() >= 6 && input.substr(4,1).compare(":") == 0) {
            int count;
            try{
                count = std::stoi(input.substr(5, std::string::npos));
            }
            catch(...) {
                output = "Invalid number of cards [<src><dst>:<num>].";
                return output;
            }
            if (!checkAndDeal(input.substr(0,2), input.substr(2,2), count))
                output = "Impossible move.";
        }
        else
            output = "Invalid command.";
    }
    return output;
}

void saveDeck(std::ofstream &file, int size, std::vector< Cards > &deck) {
    for (int i = 0; i < size; ++i) {
        file << deck[i].value << "\n";
        file << deck[i].color << "\n";

        if (deck[i].faceUp)
            file << "1\n";
        else
            file << "0\n";
    }
}

bool Game::saveGame(std::string fileName) {
    int sizeOfSavedDeck;
    std::vector< Cards > savedDeck;

    // Open file
    std::ofstream f;
    f.open(fileName);
    if (f.fail() || !f.is_open())
        return false;

    // Save main deck
    sizeOfSavedDeck = mainDeck.countAllCards();
    f << sizeOfSavedDeck << "\n";
    if (sizeOfSavedDeck != 0)
        mainDeck.getAllCards(savedDeck);
    saveDeck(f, sizeOfSavedDeck, savedDeck);
    savedDeck.clear();

    // Save the deck
    sizeOfSavedDeck = drawDeck.countAllCards();
    f << sizeOfSavedDeck << "\n";
    if (sizeOfSavedDeck != 0)
        drawDeck.getAllCards(savedDeck);
    saveDeck(f, sizeOfSavedDeck, savedDeck);
    savedDeck.clear();

    // Save piles
    for (int j = 0; j < 7; ++j) {
        sizeOfSavedDeck = tableDecks[j].countAllCards();
        f << sizeOfSavedDeck << "\n";
        if (sizeOfSavedDeck != 0)
            tableDecks[j].getAllCards(savedDeck);
        saveDeck(f, sizeOfSavedDeck, savedDeck);
        savedDeck.clear();

    }

    // Save foundations
    for (int j = 0; j < 4; ++j) {
        sizeOfSavedDeck = resultDecks[j].countAllCards();
        f << sizeOfSavedDeck << "\n";
        if (sizeOfSavedDeck != 0)
            resultDecks[j].getAllCards(savedDeck);
        saveDeck(f, sizeOfSavedDeck, savedDeck);
        savedDeck.clear();
    }

    f.close();
    return true;
}

bool loadDeck(std::ifstream &file, int size, std::vector< Cards > &deck) {
    Cards loadedCard;
    std::string line;

    for (int i = 0; i < size; ++i) {
        // Load card value
        std::getline(file, line);
        try {
            loadedCard.value = std::stoi(line);
        }
        catch(...) {
            return false;
        }

        // Load card color
        std::getline(file, line);
        try {
            loadedCard.color = std::stoi(line);
        }
        catch(...) {
            return false;
        }

        // Load faceUp info
        std::getline(file, line);
        try {
            loadedCard.faceUp = std::stoi(line) == 0 ? false : true;
        }
        catch(...) {
            return false;
        }
        deck.push_back(loadedCard);
    }
    return true;
}

bool Game::loadGame(std::string fileName) {
    std::vector< Cards > loadedDeck;
    int count;

    // Open file
    std::ifstream f;
    f.open(fileName);
    if (f.fail() || !f.is_open())
        return false;

    std::string line;

    // Load main deck
    std::getline(f, line);

    // Number of cards in main deck
    try {
        count = std::stoi(line);
    }
    catch(...) {
        return false;
    }

    if (!loadDeck(f, count, loadedDeck))
        return false;
    mainDeck.makeDeckOfCards(loadedDeck);
    loadedDeck.clear();

    // Load draw deck
    std::getline(f, line);

    // Number of cards in draw deck
    try {
        count = std::stoi(line);
    }
    catch(...) {
        return false;
    }

    if (!loadDeck(f, count, loadedDeck))
        return false;
    drawDeck.makeDeckOfCards(loadedDeck);
    loadedDeck.clear();

    // Load table decks
    for (int j = 0; j < 7; ++j) {
        std::getline(f, line);

        // Number of cards in table deck[j]
        try {
            count = std::stoi(line);
        }
        catch(...) {
            return false;
        }

        if (!loadDeck(f, count, loadedDeck))
            return false;
        tableDecks[j].makeDeckOfCards(loadedDeck);
        loadedDeck.clear();
    }

    // Load result decks
    for (int j = 0; j < 4; ++j) {
        std::getline(f, line);

        // Number of cards in result deck[j]
        try {
            count = std::stoi(line);
        }
        catch(...) {
            return false;
        }

        if (!loadDeck(f, count, loadedDeck))
            return false;
        resultDecks[j].makeDeckOfCards(loadedDeck);
        loadedDeck.clear();
    }
    return true;
}

void Game::saveAction(Action move) {
    if (steps.size() >= 10)
        steps.pop_front();
    steps.push_back(move);
}

void Game::undo() {
    if (steps.empty())
        return;
    Action step = steps.back();
    steps.pop_back();

    if (step.flip) {
        if (drawDeck.isEmpty()) {
            while (!mainDeck.isEmpty()) {
                mainDeck.dealCard(drawDeck);
                drawDeck.flipTopCard();
            }
        }
        else {
            drawDeck.flipTopCard();
            drawDeck.dealCard(mainDeck);
        }
    }
    else {
        if (step.fromType == draw) {
            if (step.toType == table)
                tableDecks[step.toIndex].dealCard(drawDeck);
            else
                resultDecks[step.toIndex].dealCard(drawDeck);
        }
        else if (step.fromType == table) {
            if (step.flippedTopCard)
                tableDecks[step.fromIndex].flipTopCard();
            if (step.toType == table) {
                tableDecks[step.toIndex].dealXCards(tableDecks[step.fromIndex],
                                                    step.count);
            }
            else
                resultDecks[step.toIndex].dealCard(tableDecks[step.fromIndex]);
        }
        else {
            tableDecks[step.toIndex].dealCard(resultDecks[step.fromIndex]);
        }
    }
}

/** End of file game.cpp **/
