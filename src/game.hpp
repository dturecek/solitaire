/**
 * @file game.hpp
 * @brief This file contains declarations of functions, structures and a class
 * for game representation.
 *
 * @author Dominik Tureček, xturec06@stud.fit.vutbr.cz
 */
#ifndef GAME_HPP
#define GAME_HPP

#include <sstream>
#include <deque>
#include "cards.hpp"

/**
 * @brief Representation of deck type.
 */
enum deckType {
    draw,  /**< Draw deck */
    table,  /**< Pile */
    result  /**< Foundation */
};

/**
 * @brief Resentation of a resolved action.
 */
struct Action {
    bool flip;  /**< Card from main deck dealt to draw deck */
    int fromType;  /**< Type of source deck in card move action */
    int fromIndex;  /**< Index of source deck in card move action */
    int toType;  /**< Type of destination deck in card move action */
    int toIndex;  /**< Index of destination deck in card move action */
    int count;  /**< Number of cards affected */
    bool flippedTopCard;  /**< Action caused flip of top card */
};

/**
 * @brief Representation of the game state.
 */
struct State {
    bool mainDeckEmpty;  /**< Main deck is empty */
    bool drawDeckEmpty;  /**< Draw deck is empty */
    bool resultDeckEmpty[4];  /**< Foundations are empty */
    int tableFaceDownCount[7];  /**< Number of face down cards in piles */
    Cards drawDeckTopCard;  /**< Top card of draw deck */
    Cards resultDecksTopCards[4];  /**< Top cards of foundations */
    Deck tableDecks[7];  /**< Piles */
};

/**
 * @brief Get full name of a given card.
 *
 * @param card Card to be named.
 * @return String containing full name of the card.
 */
std::string getCardName(Cards card);

/**
 * @brief Get identification of a deck given by name.
 *
 * @param name Name of the deck.
 * @param type Address of the integer for storing information about the type
 *             of given deck.
 * @param index Address of the integer for storing information about the index
 *              of the pile or foundation.
 * @return True if name represents a valid deck, otherwise false and 
 *         information given is not valid.
 */
bool identifyDeck(std::string name, int* type, int* index);

/**
 * @brief Check whether a vector of cards contains valid straight.
 *
 * @param arr Vector of cards.
 * @param count Number of cards to check.
 * @return True if straight is valid, otherwise false.
 */
bool checkStraight(std::vector<Cards> arr, int count);

/**
 * @brief Check combination of source and destination for a card move action.
 *
 * @param src Type of source deck represented by enum deckType.
 * @param dest Type of destination deck represented by enum deckType.
 * @param count Number of cards to be moved.
 * @param idxS Index of source deck.
 * @param idxD Index of destination deck.
 * @return True if action is valid, otherwise false.
 */
bool checkCombination(int src, int dest, int count, int idxS, int idxD);

/**
 * @brief Save information about a given deck to a given file.
 *
 * @param file File used for storing information about given deck.
 * @param size Size of deck to be stored.
 * @param deck Vector of cards to be stored.
 */
void saveDeck(std::ofstream &file, int size, std::vector<Cards> &deck);

/**
 * @brief Load information about given deck from given file.
 *
 * @param file File containing information about the deck.
 * @param size Size of the deck to be loaded.
 * @param deck Address of the vector used for information storage.
 * @return True if information is valid, otherwise false.
 */
bool loadDeck(std::ifstream &file, int size, std::vector< Cards > &deck);

/**
 * @brief Game representation.
 *
 * Class represents one game and provides methods for various actions.
 */
class Game {
    Deck mainDeck;  /**< Main deck */
    Deck drawDeck;  /**< Draw deck */
    Deck tableDecks[7];  /**< Piles */
    Deck resultDecks[4];  /**< Foundations */
    std::deque<Action> steps;  /**< Deque of resolved actions used for undo action */
    bool gameInProgress = false;  /**< Information about whether game was started */

public:
    /**
     * @brief Start the game.
     */
    void startGame();

    /**
     * @brief Print information about the current state of the game to 
     *        the standard output.
     */
    void printGame();

    /**
     * @brief Process a string command and issue a corresponding action.
     *
     * @param input Description of the command.
     * @return String describing the result of the action.
     */
    std::string handleInput(std::string input);

      /**
     * @brief Provide a card move action.
     *
     * @param src Source deck.
     * @param dst Destination deck.
     * @param count Number of cards to be moved.
     *
     * @return True in case of success, otherwise false.
     */
    bool checkAndDeal(std::string src, std::string dest, int count);

    /**
     * @brief Check winning condition.
     *
     * @return True if current state of the game represents win, otherwise
     *         false.
     */
    bool checkWin();

    /**
     * @brief Return information about a possible move in the form of a command
     *        used in console interface.
     *
     * @return String describing possible move in the form of a command used in
     *         console interface.
     */
    std::string printHint();

    /**
     * @brief Get a long description of a possible move the for graphic
     *        interface.
     *
     * @param longHint Address of the string for storing description of
     *                 a possible move.
     * @return String describing a possible move in the form of command used
     *         in console interface.
     */
    std::string printHint(std::string &longHint);

    /**
     * @brief Save game to a file given by its name.
     *
     * @param fileName Name of the file to be used for saving the game.
     * @return True if the game was successfully saved, otherwise false.
     */
    bool saveGame(std::string fileName);

    /**
     * @brief Load the game from a file given by its name.
     *
     * @param fileName Name of the file containing saved game.
     * @return True if game was successfully loaded, otherwise false.
     */
    bool loadGame(std::string fileName);

    /**
     * @brief Save resolved action to queue for possible undo action.
     *
     * @param move Description of resolved action using structure Action.
     */
    void saveAction(Action move);

    /**
     * @brief Undo action.
     */
    void undo();

    /**
     * @brief Return the current game state.
     *
     * @return Description of the current game state using structure State.
     */
    State getGameState();

    /**
     * @brief Quit the game.
     */
    void quitGame();

    /**
     * @brief Return information about whether the game is in progress.
     *
     * @return True if the game is in progress, false if game was not started.
     */
    bool inProgress();
};

#endif // GAME_HPP
/** End of file game.hpp **/
