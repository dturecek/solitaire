/**
 * @file cards.hpp
 * @brief This file contains declarations of functions, structures for card
 * representation and a class for deck representation.
 *
 * @author Dominik Tureček, xturec06@stud.fit.vutbr.cz
 */
#ifndef CARDS_HPP
#define CARDS_HPP

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cstdlib>

/**
 * @brief Card representation.
 *
 * Structure contains information about color and value of a card and whether
 * it is face up.
 */
struct Cards{
    int value;  /**< Card value */
    int color;  /**< Card color */
    bool faceUp;  /**< Card face up */
};

/**
 * @brief Representation of special card values.
 */
enum cardValue: int {
    ace = 1,  /**< Ace */
    jack = 11,  /**< Jack */
    queen = 12,  /**< Queen */
    king = 13  /**< King */
};

/**
 * @brief Representation of card colors.
 */
enum cardColor: int {
    hearts = 0,  /**< Hearts */
    diamonds = 1,  /**< Diamonds */
    spades = 2,  /**< Spades */
    clubs = 3  /**< Clubs */
};

/**
 * @brief Get a string containing an abbreviated name of given card.
 *
 * @param card Card to be named.
 * @return String containing abbreviated name of the card.
 */
std::string getCard(Cards card);

/**
 * @brief Get a pseudorandom number from interval <0, parameter i).
 *
 * @param i Upper bound of the interval.
 * @return Pseudorandom number.
 */
int randomGenerator(int i);

/**
 * @brief Deck of cards representation.
 *
 * Class represents one deck of cards and provides methods for various actions.
 */
class Deck {
    std::vector<Cards> deckOfCards;  /**< Vector representing cards in deck */

 public:
    /**
     * @brief Create a deck of 52 cards.
     */
    void createMainDeck();

    /**
     * @brief Shuffle the deck.
     */
    void shuffleDeck();

    /**
     * @brief Print information about the deck on standard output.
     */
    void printDeck();

    /**
     * @brief Deal a card from this deck to another one given by the parameter.
     *
     * @param dst Destination of the card to be dealt.
     */
    void dealCard(Deck& dst);

    /**
     * @brief Deala a number of cards from this deck to another one given by
     * the parameter.
     *
     * @param dst Destination of the cards to be dealt.
     * @param count Number of cards to be dealt.
     */
    void dealXCards(Deck& dst, int count);

    /**
     * @brief Flip the top card of the deck.
     */
    void flipTopCard();

    /**
     * @brief Get infromation about emptiness of the deck.
     *
     * @return True if the deck is empty, otherwise false.
     */
    bool isEmpty();

    /**
     * @brief Print information about the top card of the deck on standard
     * output.
     */
    void printTopCard();

    /**
     * @brief Return information about the top card of the deck.
     *
     * @return Structure filled with information about the top card of the
     * deck.
     */
    Cards getTopCard();

    /**
     * @brief Fill a given vector with information about the number of cards.
     *
     * @param count Number of cards to be given information about.
     * @param deck An address of a vector for the information to be stored in.
     * @return True if the deck contains given number of cards, otherwise false
     * and no information is stored into the given vector.
     */
    bool getXCards(int count, std::vector<Cards> &deck);

    /**
     * @brief Count face up cards in the deck.
     *
     * @return Number of face up cards.
     */
    int countFaceUpCards();

    /**
     * @brief Count all cards in the deck.
     *
     * @return Number of all cards in the deck.
     */
    int countAllCards();

    /**
     * @brief Fill a given vector with information about all cards in the deck.
     *
     * @param deck An address of a vector for the information to be stored to.
     */
    void getAllCards(std::vector<Cards> &deck);

    /**
     * @brief Create the deck from given cards.
     *
     * @param deck Vector of cards to be stored in this deck.
     */
    void makeDeckOfCards(std::vector<Cards> &deck);

    /**
     * @brief Erase all cards from the deck.
     */
    void clearDeck();
};

#endif  // CARDS_HPP
/** End of file cards.hpp **/
