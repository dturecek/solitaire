/**
 * @file main.cpp
 * @brief This file contains the main function for a game with GUI.
 *
 * The main function starts execution of the GUI application. It was generated
 * by QtCreator and edited to use singleton instance of the Main Window.
 *
 * @author Dominik Tureček, xturec06@stud.fit.vutbr.cz
 */
#include <QApplication>
#include <QMenu>
#include <QMenuBar>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
/** End of file main.cpp **/
