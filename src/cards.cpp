/**
 * @file cards.cpp
 * @brief This file contains the implementation of functions and class methods
 * for card and deck representation.
 *
 * @author Dominik Tureček, xturec06@stud.fit.vutbr.cz
 */
#include "cards.hpp"


std::string getCard(Cards card) {
    std::string color;
    std::string value;

    switch (card.color) {
        case hearts:
            color = "\xe2\x99\xa5";
            break;
        case diamonds:
            color = "\xe2\x99\xa6";
            break;
        case spades:
            color = "\xe2\x99\xa0";
            break;
        case clubs:
            color = "\xe2\x99\xa3";
            break;
    }

    switch (card.value) {
        case ace:
            value = "A";
            break;
        case jack:
            value = "J";
            break;
        case queen:
            value = "Q";
            break;
        case king:
            value = "K";
            break;
        default:
            value = std::to_string(card.value);
            break;
    }
    return (color + " " + value);
}

void Deck::createMainDeck() {
    Cards card;

    for (int i = 0; i < 4; ++i) {
        card.color = i;
        for (int j = 1; j <= 13; ++j) {
            card.value = j;
            card.faceUp = false;
            deckOfCards.push_back(card);
        }
    }
    return;
}

void Deck::printDeck() {
    int countFaceDown = 0;
    std::string printStr{""};

    if (deckOfCards.empty()) {
        std::cout << "Deck is empty";
        return;
    }

    for (auto &card : deckOfCards) {
        if (!card.faceUp) {
            countFaceDown++;
        }
        else {
            printStr = printStr + getCard(card) + "\t";
        }
    }
    if (countFaceDown == 1)
        printStr = std::to_string(countFaceDown) + " card face down\t" + printStr;
    else if (countFaceDown > 1)
        printStr = std::to_string(countFaceDown) + " cards face down\t" + printStr;

    std::cout << printStr;
}

int randomGenerator(int i) {
    srand(time(NULL));
    return rand()%i;
}

void Deck::shuffleDeck() {
    std::random_shuffle(deckOfCards.begin(), deckOfCards.end(), randomGenerator);
}

void Deck::dealCard(Deck& dst) {
    dst.deckOfCards.push_back(deckOfCards.back());
    deckOfCards.pop_back();
}

void Deck::flipTopCard() {
    if (!isEmpty()) {
        Cards& card = deckOfCards.back();
        card.faceUp = !card.faceUp;
    }
}

bool Deck::isEmpty() {
    return deckOfCards.empty();
}

void Deck::printTopCard() {
    if (!deckOfCards.empty())
        std::cout << getCard(deckOfCards.back());
    else
        std::cout << "Deck is empty";
}

Cards Deck::getTopCard() {
    return deckOfCards.back();
}

bool Deck::getXCards(int count, std::vector<Cards> &deck) {
    int size = deckOfCards.size();
    if (size < count)
        return false;

    Cards current;
    for (int i = 0; i < count; ++i) {
        current = deckOfCards[size-count+i];
        if (!current.faceUp) {
            return false;
        }

        deck.push_back(current);
    }

    return true;
}

void Deck::dealXCards(Deck& dst, int count) {
    int size = deckOfCards.size();
    for (int i = size-count; i < size; ++i) {
        dst.deckOfCards.push_back(deckOfCards[i]);
    }
    for (int i = size-count; i < size; ++i) {
        deckOfCards.pop_back();
    }
}

int Deck::countFaceUpCards() {
    int size = deckOfCards.size();
    int count = 0;
    for (int i = 0; i < size; ++i) {
        if (deckOfCards[i].faceUp)
            ++count;
    }
    return count;
}

int Deck::countAllCards() {
    return deckOfCards.size();
}

void Deck::getAllCards(std::vector< Cards > &deck) {
    int size = deckOfCards.size();
    for (int i = 0; i < size; ++i) {
        deck.push_back(deckOfCards[i]);
    }
}

void Deck::makeDeckOfCards(std::vector< Cards > &deck) {
    deckOfCards.clear();
    deckOfCards = deck;
}

void Deck::clearDeck() {
    deckOfCards.clear();
}
/** End of file cards.cpp **/
