/**
 * @file main-cli.cpp
 * @brief This file contains the main function for a game in console interface.
 *
 * The main function starts a game, prints its current state, handles input 
 * and prints information in the case of user's win.
 *
 * @author Dominik Tureček, xturec06@stud.fit.vutbr.cz
 */
#include <iostream>
#include "game.hpp"

int main () {
    Game game;
    std::string input;
    std::string output;

    game.startGame();
    game.printGame();

    while (std::getline(std::cin, input)) {
        output = game.handleInput(input);

        if (output.compare("quit") == 0)
            break;

        if (output.length() != 0)
            std::cout << output + "\n";

        game.printGame();
        if (game.checkWin()) {
            std::cout << "YOU WIN! CONGRATULATIONS!\n";
            return 0;
        }
    }
    return 0;
}
/** End of file main-cli.cpp **/
