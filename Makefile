#
#	Main Makefile for the ICP course
#	Author: Dominik Tureček
#

NAME=solitaire-cli
GUI_NAME=solitaire

all: clean
	cd src && make

clean:
	cd src && make clean
	-rm -f $(NAME) $(GUI_NAME)
	-cd doc && rm -fr *

run:
	./$(NAME) &
	./$(GUI_NAME)

doxygen:
	cd src && make doxygen

pack: clean
	zip -r xturec06.zip doc examples src Makefile README.txt
