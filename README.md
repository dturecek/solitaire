Implementation of the solitaire game for a school project.

The project is a klondike (solitaire) game implementation using both text and graphic interface.
Qt version 5.5.1 is needed to compile the graphic interface version. The GUI version is able to
run up to four games at the same time. The game can be saved to or loaded from a file - the directory
`examples` contains several saves from nearly finished games.

Text interface description:
---------------------------
**Decks' short names**
- `d` - draw deck
- `t` - pile (as in table)
- `r` - foundation (as in results)

**Possible commands:**
- `h`/`hint`              - prints a possible move (if available
- `f`/`flip`              - flips the top card in a draw deck
- `u`/`undo`              - undo action
- `dt<number>`          - moves a card from the draw deck to pile number <number>
- `dr<number>`          - moves a card from the draw deck to foundation number <number>
- `r<n1>t<n2>:1`        - moves the top card from foundation number <n1> to pile number <n2>
- `t<n1>t<n2>:<count>`  - moves <count> cards from pile number <n1> to pile number <n2>
- `t<n1>r<n2>:1`        - moves 1 card from pile number <n1> to foundation number <n2>
- `save:<file>`         - saves the game to file <file>
- `load:<file>`         - loads a game from file <file>
- `q`/`quit`              - quits the game

Available make commands
-----------------------
- `$ make`            - compiles the game
- `$ make run`        - runs both text interface and graphic interface versions
- `$ make doxygen`    - generates doxygen documentation to directory doc
- `$ make clean`      - deletes all files created during compilation (including the game and documentation)
- `$ make pack`       - creates .zip archive of all source materials
- `$ ./solitaire`     - starts graphic version
- `$ ./solitaire-cli` - starts text version

### Note
Graphic interface uses:
[Vectorized Playing Cards 2.0](http://sourceforge.net/projects/vector-cards/)
- Copyright 2015 - Chris Aguilar - conjurenation@gmail.com
- Licensed under [LGPL 3](www.gnu.org/copyleft/lesser.html)
